---
grails:
    config:
        locations:
            - file:///${user.home}/.grails/mvar/mvar_core/prod-config.yml
    profile: rest-api
    codegen:
        defaultPackage: mvar.core
    gorm:
        reactor:
            # Whether to translate GORM events into Reactor events
            # Disabled by default for performance reasons
            events: false
    cors:
        enabled: true
info:
    app:
        name: 'mvar'
        version: '0.1'
        grailsVersion: '3.3.9'
spring:
    main:
        banner-mode: "off"
    groovy:
        template:
            check-template-location: false

# Spring Actuator Endpoints are Disabled by Default
endpoints:
    enabled: false
    jmx:
        enabled: true

---
grails:
    mime:
        disable:
            accept:
                header:
                    userAgents:
                        - Gecko
                        - WebKit
                        - Presto
                        - Trident
        types:
            json:
              - application/json
              - text/json   
            hal:
              - application/hal+json
              - application/hal+xml  
            xml:
              - text/xml
              - application/xml                                 
            atom: application/atom+xml
            css: text/css
            csv: text/csv
            js: text/javascript
            rss: application/rss+xml
            text: text/plain
            all: '*/*'            
    urlmapping:
        cache:
            maxsize: 1000
    controllers:
        defaultScope: singleton
        upload:
            maxFileSize: 9999999999
            maxRequestSize: 9999999999
    converters:
        encoding: UTF-8

---
hibernate:
    cache:
        queries: false
        use_second_level_cache: false
        use_query_cache: false
dataSource:
    pooled: true
    jmxExport: true
    driverClassName: com.mysql.cj.jdbc.Driver
    dialect: org.hibernate.dialect.MySQL5InnoDBDialect
    # username:
    # password:
    configClass: org.jax.mvarcore.FieldAccessHibernateConfiguration
    properties:
        minIdle: 5
        maxIdle: 25
        maxWait: 10000
        maxAge: 10 * 60000
        timeBetweenEvictionRunsMillis: 5000
        minEvictableIdleTimeMillis: 60000
        validationQuery: "SELECT 1"
        validationQueryTimeout: 3
        validationInterval: 15000
        testOnBorrow: true
        testWhileIdle: true
        testOnReturn: false
        jdbcInterceptors: "ConnectionState;StatementCache(max=200)"
        defaultTransactionIsolation: java.sql.Connection.TRANSACTION_READ_COMMITTED
        dbProperties:
            # Mysql specific driver properties
            # http://dev.mysql.com/doc/connector-j/en/connector-j-reference-configuration-properties.html
            # let Tomcat JDBC Pool handle reconnecting
            autoReconnect: false
            # truncation behaviour
            jdbcCompliantTruncation: false
            # mysql 0-date conversion
            zeroDateTimeBehavior: 'convertToNull'
            # Tomcat JDBC Pool's StatementCache is used instead, so disable mysql driver's cache
            cachePrepStmts: false
            cacheCallableStmts: false
            # Tomcat JDBC Pool's StatementFinalizer keeps track
            dontTrackOpenResources: true
            # performance optimization: reduce number of SQLExceptions thrown in mysql driver code
            holdResultsOpenOverStatementClose: true
            # enable MySQL query cache - using server prep stmts will disable query caching
            useServerPrepStmts: false
            # metadata caching
            cacheServerConfiguration: true
            cacheResultSetMetadata: true
            metadataCacheSize: 100
            # timeouts for TCP/IP
            connectTimeout: 15000
            socketTimeout: 120000
            # timer tuning (disable)
            maintainTimeStats: false
            enableQueryTimeouts: false
            # misc tuning
            noDatetimeStringSync: true

environments:
    development:
        dataSource:
            dbCreate: update
            url: jdbc:mysql://localhost:3306/mvar_core?cachePrepStmts=true&prepStmtCacheSize=350&prepStmtCacheSqlLimit=2048&useServerPrepStmts=true&rewriteBatchedStatements=true&useUnicode=yes&characterEncoding=UTF-8&serverTimezone=UTC
            #url: jdbc:mysql://localhost:3306/mvar_core?autoReconnect=true&useUnicode=yes&characterEncoding=UTF-8&serverTimezone=UTC
        uploads:
            vcf: "${user.home}/MVAR/vcf_uploads"
    test:
        dataSource:
            dbCreate: update
            url: jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE
    production:
        dataSource:
            dbCreate: update
            url: jdbc:mysql://localhost:3306/mvar_core?useServerPrepStmts=true&rewriteBatchedStatements=true&useUnicode=yes&characterEncoding=UTF-8&serverTimezone=UTC
        uploads:
            vcf: "${user.home}/MVAR/vcf_uploads"
